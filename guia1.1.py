#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#función calcular el valor de los lapices de manera acondicionada
def valor_total():

    N = int(input("Ingrese cuantos lápices deseea comprar: "))

    if N >= 1000:
        print("Valor por su compra de:", N, "lapices es de ", N * 85, "pesos")
    else:
        print("Valor por su compra de:", N, "lapices es de ", N * 90, "pesos")

    return

if __name__ == '__main__':

    print("Precio de lápices acorde a su compra")
    precio = valor_total()
