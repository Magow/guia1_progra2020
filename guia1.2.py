#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#cantidades a comprar con un mínimo y recursividad
def compra():

    N = int(input("Ingrese cuantos trajes deseea comprar: "))

    if N < 3:
        print("\nPara poder realizar su compra debe elegir minimo 3 productos")
        print("intente nuevamente\n")
        compra()
    else:
        print("\nIndique en la caja los precios de sus trajes:\n")
        trajes = caja(N)

#calculos para definir el valor final por ofertas
def caja(N):

    lista_total = []
    precio_original = 0
    precio_final = 0
    ahorro = 0

    for i in range(N):
        print("ingrese el valor del traje n", i + 1)
        lista_total.append(int(input("precio = ")))

    for i in lista_total:
        if i > 10000:
            precio_original += i
            precio_final += i - (i * 0.15)
            ahorro += i * 0.15
        else:
            precio_original += i
            precio_final += i - (i * 0.08)
            ahorro += i * 0.08

    print("el precio sin descuentos es de: ", precio_original, "pesos")
    print("el precio con descuentos es de: ", precio_final, "pesos")
    print("el ahorro total es de: ", ahorro, "pesos")

if __name__ == '__main__':

    print("Ha ingresado a comprar trajes, elija sus modelos")
    print("Recuerde, debe comprar mínimo 3 trajes por las ofertas actuales\n")
    lista_final = compra()
