#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#algoritmos con condiciones necesarias, impresión y calculos 
def funcion_condiciones():

    num = (int(input("numero de alumnos para muestra de edades: ")))
    edades = []
    prom = 0

    for i in range(num):
        print("alumno numero: ", i + 1)
        edades.append(int(input("edad = ")))
    print("las edades de los alumnos son: ", edades)

    x = 1
    while x <= num:
        prom += edades[x - 1]
        x += 1
    print(prom)
    print(num)
    prom = (prom/num)
    print("El promedio de edades de los alumnos es de: ", prom)


if __name__ == '__main__':
    print("Ingrese las edades de los alumnos que quiera")

    num_alumnos = funcion_condiciones()
