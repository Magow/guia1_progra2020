#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#valores a usar luego (meses), y valor de ahorro inicial
def app_ahorro():

    num = 12
    meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
            'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    ahorro_mensual = []

    for i in range(num):
        print("¿Cuanto ahorró en el mes de ", meses[i], "?",)
        ahorro_mensual.append(int(input("ahorró: ")))

    revisar_ahorros(num, meses, ahorro_mensual)

#calculos de lo ahorrado con recursividad
def revisar_ahorros(num, meses, ahorro_mensual):

    revisar = None
    N = int(input("Ingrese hasta qué mes desea ver sus ahorros (indique con número): "))
    revisar = N
    ahorrado = 0

    for i in range(12):
        x = revisar
        if i < x:
            ahorrado += ahorro_mensual[i]
            print("lo ahorrado hasta el mes de ", meses[i], "es de:", ahorrado)

    print("Siendo esto un total de: ", ahorrado)

    seguir = int(input("si desea revisar hasta otro més presione '1': "))
    if seguir == 1:
        revisar_ahorros(num, meses, ahorro_mensual)
    else:
        print("\nGracias por confiarnos sus ahorros")


if __name__ == '__main__':

    print("Aplicación de ahorros mensuales")
    ahorrado = app_ahorro()
