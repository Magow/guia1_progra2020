#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#función numeros pares
def num_pares():

    print("los números pares entre '0' y '100'son: ")
    for i in range(0, 100, 2):
        if i == 0:
            continue
        else:
            print(i)

if __name__ == '__main__':

    pares = num_pares()
