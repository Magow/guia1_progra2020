#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#valores necesarios para los calculos y excepciones para fallos por horas excesivas en el día
def horas_pagos():

    dias = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado']
    sueldo_hora = 12000
    trabajo_semanal = []

    for i in range(6):
        print("Horas trabajadas el día", dias[i])
        trabajo_semanal.append(int(input("total de: ")))
        if trabajo_semanal[i] > 24:
            print("USTED HA INTENTADO ENGAÑAR AL SISTEMA")
            print("Se su despido y un sumario están siendo procesados en este momento, adiós")
            exit()
        else:
            continue
    calculos(dias, sueldo_hora, trabajo_semanal)
    
#calculo de los valores entregados junto a su impresión
def calculos(dias, sueldo_hora, trabajo_semanal):

    total = 0
    for i in range(6):
        total += trabajo_semanal[i]
    print("Ha trabajado un total de: ", total, "horas")
    print("Por lo que se le será depositado a su cuenta: ", total * sueldo_hora, "pesos")
    print("Gracias por trabajar con nosotros")

if __name__ == '__main__':

    print("Máquina de pagos semanales")
    horas_pagos()
