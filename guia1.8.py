#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#agrupación de años
def acumular_edades(N):

    spawn_de_postulantes = []

    print("Ahora ingrese los años de nacimiento de los postulantes")
    for i in range(N):
        print("Año de nacimiento postulante  n°", i + 1, " = ", end="")
        spawn_de_postulantes.append(int(input()))
    print("\n")
    calculadora_edad(N, spawn_de_postulantes)

#condiciones algo exageradas para una buena presentación de datos sin numeros negativos
#ni gente milenaria
def calculadora_edad(N, spawn_de_postulantes):

    final = N
    prom = 0
    error = 0

    for i in range(N):
        if spawn_de_postulantes[i] <= 1900:
            print("El postulante o es un fosil viviente o no sabe escribir")
            print("La edad del supuesto postulante n°", i + 1, " es de ", end="")
            print(2020 - spawn_de_postulantes[i], "años\n")
            error += (2020 - spawn_de_postulantes[i])
            final -= 1

        if spawn_de_postulantes[i] > 2002 and spawn_de_postulantes[i] <= 2020:
            print("El postulante es menor de edad o acaba de nacer ¬_¬")
            print("La edad del supuesto postulante n°", i + 1, " es de ", end="")
            print(2020 - spawn_de_postulantes[i], "años\n")
            prom += (2020 - spawn_de_postulantes[i])


        if spawn_de_postulantes[i] > 2020:
            print("Bienvenido viajero del tiempo, mal año para solicitar trabajo")
            print("La edad para el supuesto postulante n°", i + 1, " es ", end="")
            print("indefinida\n")
            error += (2020 - spawn_de_postulantes[i])
            final -= 1

        if spawn_de_postulantes[i] > 1900 and spawn_de_postulantes[i] <= 2002:
            print("La edad del postulante n°", i + 1, " es de ", end="")
            print(2020 - spawn_de_postulantes[i], "años\n")
            prom += (2020 - spawn_de_postulantes[i])

    if final > 0:
        print("El promedio de edades es de los postulantes que pueden existir es de:", prom/final)
    else:
        print("El promedio de edades no es válido para la postulación, pero es:", error/N)




if __name__ == '__main__':

    N = int(input("Ingrese cuantos postulantes se han presentado a la empresa = "))
    acumular_edades(N)
