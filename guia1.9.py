#!/usr/bin/env python3
# -*- coding:utf-8 -*-

#calculos de cobro por hora
def cobros():
    horas_p = 4850
    horas_cobro = 0

    print("¿Cuantos minutos estuvo estacionado?")
    min_est = int(input("minutos = "))
#revision si existen valores decimales (minutos)
    if min_est % 60 != 0:
        horas_cobro = (min_est//60) + 1
        print("El cobro por un total de:", horas_cobro, "horas es de ", end="")
        print(horas_cobro * 4850, "pesos")
        print("no hay derecho a quejas ;]")
    else:
        horas_cobro = (min_est//60)
        print("El cobro por un total de:", horas_cobro, "horas es de ", end="")
        print(horas_cobro * 4850, "pesos")


if __name__ == '__main__':
    print("Bienvenido al estacionamiento central, le cobraremos de manera justa")
    print("Solo cobramos por hora así que si se queda atrapado en el taco por más tiempo va a pagar igual")
    cobros()
